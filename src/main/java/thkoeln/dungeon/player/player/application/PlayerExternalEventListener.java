package thkoeln.dungeon.player.player.application;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.context.event.EventListener;
import org.springframework.messaging.handler.annotation.Header;
import org.springframework.stereotype.Service;

import thkoeln.dungeon.player.core.domainprimitives.command.Command;
import thkoeln.dungeon.player.core.domainprimitives.purchasing.Money;
import thkoeln.dungeon.player.core.events.AbstractEvent;
import thkoeln.dungeon.player.core.events.EventFactory;
import thkoeln.dungeon.player.core.events.EventHeader;
import thkoeln.dungeon.player.core.events.concreteevents.game.RoundStatusEvent;
import thkoeln.dungeon.player.core.events.concreteevents.game.RoundStatusType;
import thkoeln.dungeon.player.core.events.concreteevents.planet.PlanetDiscoveredEvent;
import thkoeln.dungeon.player.core.events.concreteevents.robot.reveal.RobotRevealedDto;
import thkoeln.dungeon.player.core.events.concreteevents.robot.spawn.RobotSpawnedEvent;
import thkoeln.dungeon.player.core.events.concreteevents.robot.mine.RobotResourceMinedEvent;
import thkoeln.dungeon.player.core.events.concreteevents.robot.move.RobotMovedEvent;

import thkoeln.dungeon.player.core.events.concreteevents.robot.reveal.RobotsRevealedEvent;
import thkoeln.dungeon.player.core.events.concreteevents.trading.BankAccountClearedEvent;
import thkoeln.dungeon.player.core.events.concreteevents.trading.BankAccountTransactionBookedEvent;
import thkoeln.dungeon.player.core.events.concreteevents.trading.BankInitializedEvent;
import thkoeln.dungeon.player.core.restadapter.GameServiceRESTAdapter;
import thkoeln.dungeon.player.player.domain.Player;
import thkoeln.dungeon.player.player.domain.PlayerRepository;
import thkoeln.dungeon.player.robot.application.RobotApplicationService;
import thkoeln.dungeon.player.robot.domain.Robot;

@Service
public class PlayerExternalEventListener {
    private Logger logger = LoggerFactory.getLogger( PlayerExternalEventListener.class);
    private EventFactory eventFactory;
    private ApplicationEventPublisher applicationEventPublisher;
    private PlayerApplicationService playerApplicationService;
    private PlayerCommandBuilder playerCommandBuilder;
    private RobotApplicationService robotApplicationService;
    
    

    @Autowired
    public PlayerExternalEventListener( EventFactory eventFactory,
                                ApplicationEventPublisher applicationEventPublisher,
                                PlayerApplicationService playerApplicationService,
                                PlayerCommandBuilder playerCommandBuilder,
                                RobotApplicationService robotApplicationService
                                
    ) {
        this.eventFactory = eventFactory;
        this.applicationEventPublisher = applicationEventPublisher;
        this.playerApplicationService = playerApplicationService;
        this.playerCommandBuilder = playerCommandBuilder;
        this.robotApplicationService = robotApplicationService;

        
    }


    /**
     * Listener to all events that the core services send to the player
     * @param eventIdStr
     * @param transactionIdStr
     * @param playerIdStr
     * @param type
     * @param version
     * @param timestampStr
     * @param payload
     */
    @RabbitListener( queues = "player-${dungeon.playerName}" )
    public void receiveEvent( @Header( required = false, value = EventHeader.EVENT_ID_KEY ) String eventIdStr,
                              @Header( required = false, value = EventHeader.TRANSACTION_ID_KEY ) String transactionIdStr,
                              @Header( required = false, value = EventHeader.PLAYER_ID_KEY ) String playerIdStr,
                              @Header( required = false, value = EventHeader.TYPE_KEY ) String type,
                              @Header( required = false, value = EventHeader.VERSION_KEY ) String version,
                              @Header( required = false, value = EventHeader.TIMESTAMP_KEY ) String timestampStr,
                              String payload ) {
        try {
            EventHeader eventHeader =
                    new EventHeader( type, eventIdStr, playerIdStr, transactionIdStr, timestampStr, version );
            AbstractEvent newEvent = eventFactory.fromHeaderAndPayload( eventHeader, payload );                 //Factory Umwandlung



            logger.info( "======== EVENT =====> " + newEvent.toStringShort() + "\n"  );
            logger.debug( "======== EVENT (detailed) =====>\n" + newEvent );
            if ( !newEvent.isValid() ) {
                logger.warn( "Event invalid: " + newEvent );
                return;
            }
            else {
                this.applicationEventPublisher.publishEvent(newEvent);

                
            }
        }
        catch ( Exception e ) {
            logger.error ( "!!!!!!!!!!!!!! EVENT ERROR !!!!!!!!!!!!!\n" + e );
        }
    }



    @EventListener(BankInitializedEvent.class)
    public void getMonetenDetails(BankInitializedEvent bankInitializedEvent){
        playerApplicationService.updateMoney(bankInitializedEvent.getBalance());

    }

    @EventListener(RoundStatusEvent.class)
    public void getRoundStatus(RoundStatusEvent roundStatusEvent){
        switch (roundStatusEvent.getRoundStatus()) {
            case STARTED:
                logger.info("-----------------------------STARTED-ROUND-----------------------------");
                
                playerCommandBuilder.runCommands();

                break;
            case COMMAND_INPUT_ENDED:
                logger.info("-----------------------------COMMAND-INPUT-ENDED-----------------------------");

                break;
            case ENDED:
                logger.info("-----------------------------ROUND-ENDED-----------------------------");

                break;
        
            default:
                break;
        }
    }

    @EventListener(RobotsRevealedEvent.class)
    public void robotsReveal(RobotsRevealedEvent robotsRevealedEvent){
        RobotRevealedDto[] roboRevealArray = robotsRevealedEvent.getRobots();
        logger.info("I--->Robots: " + roboRevealArray.length);
        for (RobotRevealedDto robotRevealedDto : roboRevealArray) {
            logger.info("Robot ID: " + robotRevealedDto.getRobotId() + "\nPlanetID: " + robotRevealedDto.getPlanetId() + "\nHealth: " + robotRevealedDto.getHealth() + "\nEnergy: " + robotRevealedDto.getEnergy()+"\n\n");
        }
    }

    @EventListener(BankAccountTransactionBookedEvent.class)
    public void transactionBookedEvent(BankAccountTransactionBookedEvent bankAccountTransactionBookedEvent){
        playerApplicationService.updateMoney(bankAccountTransactionBookedEvent.getBalance());
    }

    @EventListener(RobotSpawnedEvent.class)
    public void RobotSpawnedEvent(RobotSpawnedEvent roboSpawn){
        playerApplicationService.spawnRobot(roboSpawn);
    }

    @EventListener(RobotMovedEvent.class)
    public void RobotMovedEvent(RobotMovedEvent robotMovedEvent){
        robotApplicationService.robotMoved(robotMovedEvent);
    }

    @EventListener(PlanetDiscoveredEvent.class)
    public void PlanetDiscoveredEvent(PlanetDiscoveredEvent planetDiscoveredEvent){
        robotApplicationService.PlanetDiscovered(planetDiscoveredEvent);;
    }

    @EventListener(RobotResourceMinedEvent.class)
    public void RobotMinedEvent(RobotResourceMinedEvent robotResourceMinedEvent){
        robotApplicationService.RobotMinedEvent(robotResourceMinedEvent);
    }





    @EventListener(BankAccountClearedEvent.class)
    public void BankAccountClearEvent(BankAccountClearedEvent bankAccountClear){
        playerApplicationService.resetPlayerBalance(bankAccountClear.getBalance());
    }

    @EventListener(Error.class)
    public void ErrorEvent(Error err){
        logger.warn(err.toString());
    }
}
