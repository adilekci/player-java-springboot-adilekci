
package thkoeln.dungeon.player.player.application;

import org.hibernate.mapping.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


import lombok.extern.slf4j.Slf4j;
import thkoeln.dungeon.player.core.domainprimitives.command.Command;
import thkoeln.dungeon.player.core.restadapter.GameServiceRESTAdapter;
import thkoeln.dungeon.player.player.domain.Player;
import thkoeln.dungeon.player.robot.domain.Robot;

@Service
@Slf4j
class PlayerCommandBuilder {

    private PlayerApplicationService playerApplicationService;
    private GameServiceRESTAdapter gameServiceRESTAdapter;
    
    @Autowired
    public PlayerCommandBuilder(PlayerApplicationService playerApplicationService, GameServiceRESTAdapter gameServiceRESTAdapter ){
        this.playerApplicationService = playerApplicationService;
        this.gameServiceRESTAdapter = gameServiceRESTAdapter;
    }

    public void runCommands() {
        Player player = playerApplicationService.queryAndIfNeededCreatePlayer();
        java.util.List<Robot> playerRobots = player.getRobots();
        player.getNextCommand().clear();
        player.buyRobots();

        for(Robot robot : playerRobots){
            if(robot.getRobotPlanet().minableRessourceAvailable()){
                player.mine(robot);
            }
            
            else{
                player.moveRobots(robot);

            }
        }

        
        
        playerApplicationService.savePlayer(player);
        for ( Command command : player.getNextCommand()){
            gameServiceRESTAdapter.sendPostRequestForCommand(command);
        }
        log.info("Submitted: " + player.getNextCommand().size() + " Commands to Game!");
    }
    
}