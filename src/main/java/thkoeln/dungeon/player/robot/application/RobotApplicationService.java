package thkoeln.dungeon.player.robot.application;

import java.util.UUID;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import lombok.extern.slf4j.Slf4j;
import thkoeln.dungeon.player.core.events.concreteevents.planet.PlanetDiscoveredEvent;
import thkoeln.dungeon.player.core.events.concreteevents.planet.PlanetNeighboursDto;
import thkoeln.dungeon.player.core.events.concreteevents.planet.PlanetResourceDto;
import thkoeln.dungeon.player.core.events.concreteevents.robot.mine.RobotResourceInventoryDto;
import thkoeln.dungeon.player.core.events.concreteevents.robot.mine.RobotResourceMinedEvent;
import thkoeln.dungeon.player.core.events.concreteevents.robot.move.RobotMovedEvent;
import thkoeln.dungeon.player.core.events.concreteevents.robot.spawn.RobotDto;
import thkoeln.dungeon.player.core.events.concreteevents.robot.spawn.RobotSpawnedEvent;
import thkoeln.dungeon.player.robot.domain.Robot;
import thkoeln.dungeon.player.robot.domain.RobotException;
import thkoeln.dungeon.player.robot.domain.RobotPlanet;
import thkoeln.dungeon.player.robot.domain.RobotPlanetRessourceDto;
import thkoeln.dungeon.player.robot.domain.RobotRepository;
import thkoeln.dungeon.player.robot.domain.RobotResourceInventory;

@Service
@Slf4j
public class RobotApplicationService {
    private RobotRepository robotRepository;

    @Autowired
    public RobotApplicationService(RobotRepository robotRepository){
        this.robotRepository = robotRepository;
    }

    public Robot spawnRobot(RobotSpawnedEvent robotSpawnedEvent){
        if(!robotSpawnedEvent.isValid()){
            log.warn("RobotspawnedEvent invalid: " + robotSpawnedEvent);
        }
        RobotDto robotDto = robotSpawnedEvent.getRobotDto();
        Robot robot = new Robot(robotDto.getId(), robotDto.getPlanet().getPlanetId());
        robotRepository.save( robot);
        log.info("RobotSpawned: "+ robot);
        return robot;

    }
    public void PlanetDiscovered(PlanetDiscoveredEvent planetDiscoverEvenet)
    {
        if (!planetDiscoverEvenet.isValid()) {
            
            throw new RobotException("PlanetDiscoveredEvent is invalid: "+ planetDiscoverEvenet);
        }

        PlanetResourceDto ressourceDto = planetDiscoverEvenet.getResource();
        RobotPlanetRessourceDto robotPlanetRessourceDto = RobotPlanetRessourceDto.noInfoDto();
        if (ressourceDto != null) {
            
            robotPlanetRessourceDto = new RobotPlanetRessourceDto(ressourceDto.getResourceType(), ressourceDto.getCurrentAmount(), ressourceDto.getMaxAmount());


        }
        


        java.util.List<Robot> robotsOnPlanet = robotRepository.findByRobotPlanetPlanetId(planetDiscoverEvenet.getPlanetId());
        if (robotsOnPlanet.isEmpty()) {
            log.info("There are no Robots on this tile "+planetDiscoverEvenet.getPlanetId());
        }
        PlanetNeighboursDto[] planetNeighboursDtos = planetDiscoverEvenet.getNeighbours();
        for(Robot robot : robotsOnPlanet){
            RobotPlanet updatedRobotPlanet = RobotPlanet.planetWithNeighbours(planetDiscoverEvenet.getPlanetId(), planetNeighboursDtos);
            robot.setRobotPlanet(updatedRobotPlanet);
            robot.getRobotPlanet().setRobotPlanetRessourceDto(robotPlanetRessourceDto);
            robotRepository.save(robot);
            log.info("RobotPlanet Updated "+updatedRobotPlanet.getPlanetId());
        }

    }

    public void robotMoved(RobotMovedEvent robotMovedEvent){
        UUID robotUUID = robotMovedEvent.getRobotId();
        if(robotUUID == null){
            log.warn("robotUUID NULL cant be found to update robotMoved");
            return;
        }
        UUID updatedPlanetUUID = robotMovedEvent.getToPlanet().getId();
        if(updatedPlanetUUID == null){
            log.warn("updatedPlanetUUID NULL cant be found to update robotMoved");
            return;
        }
        Robot robotThatMoved = robotRepository.findByRobotId(robotUUID);
        RobotPlanet rp = RobotPlanet.planetWithoutNeightbours(updatedPlanetUUID);
        robotThatMoved.setRobotPlanet(rp);
        robotRepository.save(robotThatMoved);

    }

    public void RobotMinedEvent(RobotResourceMinedEvent robotResourceMinedEvent) {
        Robot robot = robotRepository.findByRobotId(robotResourceMinedEvent.getRobotId());
        String type = robotResourceMinedEvent.getMinedResource();
        int amount = robotResourceMinedEvent.getMinedAmount();
        if(robot == null){
            log.warn("MINEDEVENT Exception");
        }
        else{
            
            

            RobotResourceInventory robotInv = new RobotResourceInventory();

            


            robot.setRobotResourceInventory(robotInv);
        }
        robotRepository.save(robot);
    }
}
