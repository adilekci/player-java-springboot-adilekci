package thkoeln.dungeon.player.robot.domain;

import java.util.UUID;


import jakarta.persistence.Embedded;
import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import thkoeln.dungeon.player.core.events.concreteevents.robot.spawn.RobotInventoryDto;

@Entity
@Slf4j
@Getter
@Setter (AccessLevel.PROTECTED)
@NoArgsConstructor( access = AccessLevel.PROTECTED)
public class Robot {
    @Id
    private final UUID id= UUID.randomUUID();

    private UUID robotId;




    @Embedded
    @Setter
    private RobotPlanet robotPlanet = RobotPlanet.nullPlanet();

    @Setter
    @Embedded
    private RobotResourceInventory robotResourceInventory = new RobotResourceInventory();

    public Robot( UUID robotId, UUID planetId){
        if ( robotId == null || planetId == null) log.info("ERROR-----NULL VALUES IN ROBOT");;
        this.robotId = robotId;
        this.robotPlanet = RobotPlanet.planetWithoutNeightbours(planetId);
    }

    public static Robot of(UUID robotId, UUID planetId){
        Robot robot = new Robot(robotId,planetId);
        return robot;
    }
    
}
