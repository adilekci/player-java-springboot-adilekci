package thkoeln.dungeon.player.robot.domain;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;


@Getter
@Setter
@NoArgsConstructor
public class RobotResourceInventory {
    @JsonProperty( "COAL" )
    private int coal = 0;
    @JsonProperty( "IRON" )
    private int iron = 0;
    @JsonProperty( "GEM" )
    private int gem = 0;
    @JsonProperty( "GOLD" )
    private int gold = 0;
    @JsonProperty( "PLATIN" )
    private int platin = 0;


    public void add(String type, int amount){
        switch (type) {
            case "COAL":
                coal += amount;
                break;
            case "IRON":
                iron += amount;
                break;
            case "GEM":
                gem += amount;
                break;
            case "GOLD":
                gold += amount;
                break;
            case "PLATIN":
                platin += amount;
                break;
        
            default:
                break;
        }
    }

    public void isTimeToSell(){
        //implement selling
    }
}
