package thkoeln.dungeon.player.robot.domain;

import static thkoeln.dungeon.player.core.domainprimitives.location.MineableResourceType.COAL;

import jakarta.persistence.Embeddable;

import lombok.Getter;
import lombok.NoArgsConstructor;
import thkoeln.dungeon.player.core.domainprimitives.location.MineableResourceType;

@Embeddable
@Getter
@NoArgsConstructor
public class RobotPlanetRessourceDto {
    private MineableResourceType resourceType;
    private Integer maxAmount;
    private Integer currentAmount;

    public RobotPlanetRessourceDto(MineableResourceType resourceType, Integer maxAmount, Integer currentAmount){
        this.resourceType = resourceType;
        this.maxAmount = maxAmount;
        this.currentAmount = currentAmount;
        
    }

    public boolean isMinableWithoutMineLevel(){

        if (resourceType == COAL && currentAmount > 0) {
            return true;
        }
        return false;
    }

    public static RobotPlanetRessourceDto noInfoDto(){
        return new RobotPlanetRessourceDto(null,null,null);
    }
    
}
