package thkoeln.dungeon.player.robot.domain;

import java.util.List;
import java.util.UUID;

import org.springframework.data.repository.CrudRepository;

public interface RobotRepository extends CrudRepository<Robot, UUID>{
    List<Robot> findByRobotPlanetPlanetId(UUID planetId);
    Robot findByRobotId(UUID id);
}
