package thkoeln.dungeon.player.robot.domain;

import static thkoeln.dungeon.player.core.domainprimitives.location.MineableResourceType.COAL;


import java.util.Arrays;
import java.util.List;
import java.util.Objects;

import java.util.UUID;
import java.util.stream.Collectors;

import org.hibernate.annotations.JdbcTypeRegistration;

import jakarta.persistence.Embeddable;
import jakarta.persistence.OneToOne;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import thkoeln.dungeon.player.core.events.concreteevents.planet.PlanetNeighboursDto;
import thkoeln.dungeon.player.core.events.concreteevents.planet.PlanetResourceDto;
import thkoeln.dungeon.player.core.events.concreteevents.robot.spawn.RobotPlanetDto;



@Getter
@AllArgsConstructor
@NoArgsConstructor (access = AccessLevel.PROTECTED)

public class RobotPlanet {
    private UUID planetId;
    private UUID northNeightbourId;
    private UUID eastNeightbourId;
    private UUID southNeightbourId;
    private UUID westNeightbourId;
    
    @Setter
    @Getter
    private RobotPlanetRessourceDto RobotPlanetRessourceDto = thkoeln.dungeon.player.robot.domain.RobotPlanetRessourceDto.noInfoDto();


    public RobotPlanet(UUID planetId, UUID northNeightbourId, UUID eastNeightbourId, UUID southNeightbourId, UUID westNeightbourId ){
        this.planetId = planetId;
        this.northNeightbourId = northNeightbourId;
        this.eastNeightbourId = eastNeightbourId;
        this.westNeightbourId = westNeightbourId;
    }
    

    public UUID randomNonNullNeighbourId(){
        UUID[] neighbours = {northNeightbourId,eastNeightbourId,southNeightbourId,westNeightbourId};

        List<UUID> nonNullNeighbours = Arrays.stream(neighbours).filter(Objects::nonNull).collect(Collectors.toList());
        if(nonNullNeighbours.isEmpty())
        {
            return null;
        }
        return nonNullNeighbours.get((int) (Math.random() * nonNullNeighbours.size()));
    }
    
    public static RobotPlanet nullPlanet() {
        return new RobotPlanet(null, null, null, null, null );
    }

    public static RobotPlanet planetWithoutNeightbours(UUID planetId){
        return new RobotPlanet(planetId, null, null, null, null);
    }

    public static RobotPlanet planetWithNeighbours(UUID planetId, PlanetNeighboursDto[] neighbours){


        UUID northNeightbourId = null;
        UUID eastNeightbourId = null;
        UUID southNeightbourId = null;
        UUID westNeightbourId = null;

        for( PlanetNeighboursDto neighbour : neighbours){
            switch (neighbour.getDirection()) {
                case NORTH:
                    northNeightbourId = neighbour.getId();
                    break;
            
                case EAST:
                    eastNeightbourId = neighbour.getId();
                    break;
                case SOUTH:
                    southNeightbourId = neighbour.getId();
                    break;
                case WEST:
                    westNeightbourId = neighbour.getId();
                    break;    
                default:
                    break;
            }
        }
        return new RobotPlanet(planetId, northNeightbourId, eastNeightbourId,southNeightbourId, westNeightbourId);
    }


    public boolean minableRessourceAvailable(){
        if(RobotPlanetRessourceDto == null) return false;
        return RobotPlanetRessourceDto.isMinableWithoutMineLevel();
    }
}
