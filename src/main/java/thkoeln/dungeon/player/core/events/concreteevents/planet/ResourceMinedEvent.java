package thkoeln.dungeon.player.core.events.concreteevents.planet;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import thkoeln.dungeon.player.core.events.AbstractEvent;

import java.util.UUID;

@Getter
@Setter
@NoArgsConstructor
public class ResourceMinedEvent extends AbstractEvent {
    private UUID planet;
    private int minedAmount = 0;
    private PlanetResourceDto resource;
    





    @Override
    public boolean isValid() {
        if ( planet == null ) return false;
        if ( minedAmount <= 0 ) return false;
        if ( resource == null ) return false;
        
        return true;
    }
}
