package thkoeln.dungeon.player.core.domainprimitives.location;

public enum MineableResourceType {
    COAL,
    IRON,
    GEM,
    GOLD,
    PLATIN
}
